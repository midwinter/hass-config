# Craig's Home Assistant Config
This is my configuration for Home Assistant!

## Screenshots
### Main
![Main Page](https://gitlab.com/wardcraigj/hass-config/raw/master/images/main.jpg "Main")
### Plants
![Plants](https://gitlab.com/wardcraigj/hass-config/raw/master/images/plants.jpg "Plants")
### Status
![Status](https://gitlab.com/wardcraigj/hass-config/raw/master/images/status.jpg "Status")
### Config
![Config](https://gitlab.com/wardcraigj/hass-config/raw/master/images/config.jpg "Config")
### Media
![Media](https://gitlab.com/wardcraigj/hass-config/raw/master/images/media.jpg "Media")