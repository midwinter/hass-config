from homeassistant.const import TEMP_CELSIUS
from homeassistant.helpers.entity import Entity
from homeassistant.helpers.entity_component import EntityComponent
import requests
import logging

REQUIREMENTS = ['requests>=2.18.4']
_LOGGER = logging.getLogger(__name__)
ATTR_SENSOR_ENTITY = 'entity_id'
OPENHAB_DOMAIN = 'sensor'


def setup_platform(hass, config, add_devices, discovery_info=None):
    """Setup the sensor platform."""

    devices = []
    component = EntityComponent(_LOGGER, OPENHAB_DOMAIN, hass)

    response = requests.get('{host}/rest/items'.format(
        host=config.get('host')))

    try:    
        for item in response.json():
            if item['type'] in ['Number', 'String']:
                devices.append(OpenhabSensor(item))
        component.add_entities(devices)
        hass.data[OPENHAB_DOMAIN] = devices
    except:
        _LOGGER.warning(response.body)
        raise

    def set_number(call):

        entity_id = call.data.get(ATTR_SENSOR_ENTITY, None)
        number = call.data.get('number', None)
        found_device = None
        all_devices = []

        for device in hass.data[OPENHAB_DOMAIN]:
            if device.entity_id == entity_id:
                req = requests.post(device.link, data=number)
                if req.status_code != requests.codes.ok:
                    req.raise_for_status()

    hass.services.register(OPENHAB_DOMAIN, 'set_number', set_number)

    return True


class OpenhabSensor(Entity):
    def __init__(self, item):
        self._item = item

    @property
    def name(self):
        """Return the name of the sensor."""
        return self._item['name']

    @property
    def state(self):
        """Return the state of the sensor."""
        return self._item['state']

    @property
    def link(self):
        """Return the state of the sensor."""
        return self._item['link']

    def update(self):
        self._item = requests.get(self._item['link']).json()
        return self._item['state']

    # @property
    # def unit_of_measurement(self):
    #     """Return the unit of measurement."""
    #     return TEMP_CELSIUS

    @property
    def should_poll(self):
        """Polling is needed."""
        return True
