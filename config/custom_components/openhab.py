"""Component for the Goalfeed service.

For more details about this platform, please refer to the documentation at
https://home-assistant.io/components/goalfeed/
"""
import json

import requests
import voluptuous as vol

import homeassistant.helpers.config_validation as cv
from homeassistant.const import CONF_PASSWORD, CONF_USERNAME

DOMAIN = 'openhab'


def setup(hass, config):

    return True
